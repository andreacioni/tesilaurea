# Data Warehouse Toolkit references

 - role_playing_dimension : 85, 206
 - multivalued_dimension_and_bridge_tables: 99, 281, 311, 323
 - null_in_fact_table: 78
 - null_attributes_in_dimension: 84

# Online references

 - http://cabibbo.dia.uniroma3.it/dw/
 - [Design Tip #166 Potential Bridge (Table) Detours](https://www.kimballgroup.com/2014/05/design-tip-166-potential-bridge-table-detours/)
 - [Many-to-Many relationships using SQL graph](https://www.sqlshack.com/replace-bridge-tables-data-warehouse-sql-server-2017-graph-database/)
 - [Solving many-to-many relation in start schema](https://www.researchgate.net/profile/Il-Yeol_Song/publication/220841965_An_Analysis_of_Many-to-Many_Relationships_Between_Fact_and_Dimension_Tables_in_Dimensional_Modeling/links/02e7e52bdb0e76ba25000000/An-Analysis-of-Many-to-Many-Relationships-Between-Fact-and-Dimension-Tables-in-Dimensional-Modeling.pdf)