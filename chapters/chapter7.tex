\chapter{Appendice}
In questo capitolo sono raccolte alcune sezioni dedicate a trattare argomenti di carattere più pratico che teorico. Si è deciso di omettere tutti file che contengono le istruzioni MySQL per la trasformazione del database HepData originale. Data la dimensione dei file in questione, \textbf{per rispettare l'ambiente}, per tutti i documenti e le query esposte in questa tesi (che per inciso avrebbero necessitato di più di un centinaio di pagine) si preferisce fornire il seguente indirizzo: \url{https://bitbucket.org/andreacioni/minehep}, dove il lettore interessato troverà il repository Git contenente tutto il lavoro svolto nonch\'e il \emph{docker-compose.yaml} per creare in locale la propria istanza \textbf{MineHep}.

\section{Caricamento dump su MySQL}
\label{appendix_dump_loading}
Il file contenente il \emph{dump} è compresso utilizzando l'algoritmo di compressione GZIP\footnote{\url{http://www.gzip.org/}} e questo significa che prima di importarlo con la tecnica esposta nelle prossime righe, è necessario rinominarlo da \emph{hepdatapublic.dmp.gz\_01Apr18} in \emph{hepdatapublic.dmp.gz}. Successivamente sarà possibile decomprimerlo utilizzando programmi come 7Zip, ottenendo così il file \emph{hepdatapublic.dmp} 

\paragraph{Nota} \label{type_to_engine_replace} Il file estratto, così com'è non è importabile da versioni recenti di MySQL \cite{mysql_type_engine_deprecation}, per ovviare all'inconveniente è necessario eseguire da console il seguente comando:
\begin{lstlisting}[language=bash]
	sed -i -e 's/TYPE=MyISAM/ENGINE=MyISAM/g' hepdatapublic.dmp
\end{lstlisting}
Per analizzare il contenuto di questo file è stato necessario dotarsi di un paio di strumenti software:
\begin{itemize}
	\item \emph{MySQL Server}: per caricare il \emph{dump} e poter eseguire query
	\item \emph{MySQL Workbench}: anche se non è l'unico \emph{tool} per connettersi e lavorare con un database MySQL, è stato comodo per alcune sue funzionalità (come la possibilità di effettuare \emph{Reverse Engineering} su un database già esistente).
\end{itemize}

Per l'installazione e configurazione del primo componente si è preferito l'utilizzo della piattaforma Docker così da avere un unico metodo standard per il \emph{setup} anche su sistemi operativi diversi. Per ottenere un'istanza di MySQL attraverso Docker basterà aprire il proprio terminale preferito ed eseguire il comando \cite{mysql_docker_hub}:
\begin{lstlisting}[language=bash]
	docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=minehep -d -p3306:3306 mysql:5.7.23
\end{lstlisting}
che produrrà come output qualcosa di simile a quello riportato in Figura \ref{sample_basic_mysql_docker}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.35]{img/sample_basic_mysql_docker.png}
	\caption{Installazione MySQL attraverso docker}
	\label{sample_basic_mysql_docker}
\end{figure}

Fatto questo avremo a disposizione un \emph{container} (il cui ID è: \emph{7ea6ea1b3dd}\dots) Docker in cui è installato \emph{MySQL Server} e possiamo quindi passare ad importare il database. Per fare questo è necessario recarsi al seguente indirizzo: \url{http://bit.ly/2TKRlWh} e scaricare l'ultima versione di MySQL Workbench. Una volta fatto, avviamolo e configuriamo la connessione con questi parametri:
\begin{itemize}
	\item Hostname: 127.0.0.1
	\item Port 3306
	\item Password: minehep
\end{itemize}
Avviando la connessione e aprendo il menù \emph{Server} e cliccando su \emph{Data Import} si aprirà una tab per il caricamento del \emph{dump}.
Selezionare \emph{Import from Self-Contained File} e inserire il nome del nuovo schema attraverso il pulsante \emph{New}, infine avviare la procedura cliccando sul tasto \emph{Start Import}. 

Una volta fatto tutto ciò abbiamo una copia identica del database che supportava il vecchio portale HepData, con il vantaggio che posiamo eseguire qualunque query SQL su di esso senza le limitazioni che ci poneva l'interfaccia Web.

\section{Avvio processo ETL}

\subsection{Importazione automatica}
\label{auto_import}
Per usufruire dell'automatismo fornito da Docker è necessario scaricare i file dal sito \url{https://bitbucket.org/andreacioni/minehep} \cite{minhep_bitbucket}, recarsi nella cartella radice che contiene il file \emph{docker-compose.yaml} ed eseguire da riga di comando il seguente:
\begin{lstlisting}[language=bash]
	docker-compose up
\end{lstlisting}
In questo modo, dopo alcune ore (a seconda della propria macchina) si avrà a disposizione un'istanza MySQL con i seguenti \emph{schema} già caricati e pronti per essere esplorati:
\begin{itemize}
    \item hepdata: database originale caricato utilizzando il \emph{dump} di HepData
    \item minehep: nuovo database con schema a stella (risolve relazioni molti-a-molti decomponendole in relazioni uno-a-molti)
    \item minehep\_bridge\_solution: nuovo database con schema a stella (risolve relazioni molti-a-molti utilizzando le \emph{bridge tables})
\end{itemize}

\subsection{Importazione manuale}
\label{manual_import}
Nel caso lo si voglia è comunque possibile avviare la procedura di importazione manualemente eseguendo gli script contenuti nella cartella \emph{mysql/}. I file con estensione \emph{*.sql} devono essere eseguiti \textbf{in serie} e \textbf{in ordine numerico} partendo da \emph{1\_fix\_db.sql} e arrivando \emph{7\_drop\_hepdata.sql}. I file in totale sono 7:
\begin{itemize}
    \item \emph{1\_fix\_db.sql}: si occupa della parte di rimozione tabelle, colonne, chiavi nonch\'e di uniformare i dati (Capitolo \ref{analyizing_source_data}).
    \item \emph{2\_create\_schemas.sql}: crea i due schemi \emph{minehep} e \emph{minehep\_bridge\_solution}.
    \item \emph{3\_create\_dimensions.sql}: crea le dimensioni e le tabelle \emph{NewOldId} che servono a rimappare vecchi e nuovi ID nel nuovo schema a stella (sono usate nella fase di creazione delle \emph{link tables}) (Capitolo \ref{examine_dimension_chapther}).
    \item \emph{4\_link\_data.sql}: creazione di tutte le tabelle temporanee di collegamento (Capitolo \ref{examine_link_tables_chapther}).
    \item \emph{5\_create\_fact\_table.sql}: creazione della tabella dei fatti (Capitolo \ref{examine_facts_chapther}).
    \item \emph{6\_create\_bridges.sql}: creazione delle \emph{bridge table} nello schema \emph{minehep\_bridge\_solution} (Capitolo \ref{un_caso_particolare}).
    \item \emph{7\_drop\_hepdata.sql}: (opzionale) rimuove hepdata per risparmiare spazio sul disco.
\end{itemize}

Sempre nella stessa cartella è presente il \emph{dump} che deve essere caricato \textbf{prima di lanciare gli script} secondo quanto già illustrato in \ref{type_to_engine_replace}.

\section{Aggiornamento script}
\label{scripts_update}
Ogni qual volta si abbia la necessità di modificare uno degli script di importazione si dovrà procedere ad un nuovo caricamento ex novo. Per fare questo non basterà eseguire il comando \emph{docker-compose down} in quanto i database caricati si trovano in un volume e l'immagine MySQL è già stata costruita (il fatto che cambino i file SQL non basta a Docker per capire che deve eseguire nuovamente la build). Eseguiamo il comando:
\begin{lstlisting}[language=bash]
    docker image rm minehep_mysql && docker-compose down -v && docker-compose up
\end{lstlisting}
per eliminare i dati sul volume e sull'immagine.