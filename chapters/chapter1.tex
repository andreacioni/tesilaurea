\chapter{Introduzione}

MineHep è un progetto innovativo nell’ambito della fisica delle alte energie che ha l’obiettivo di fornire una serie di strumenti software a supporto dell’archiviazione e dell’analisi dei dati raccolti nel corso degli anni da tutti gli esperimenti del settore.
\newline
\newline
Grazie alla collaborazione interdisciplinare tra il Dipartimento di Matematica e Informatica e il Dipartimento di Fisica e Astronomia dell’Università di Firenze, si è studiato come le tecnologie di data warehouse (DW), normalmente applicate ai processi di business aziendale, potessero essere applicate al nostro settore di interesse. In questa tesi è stata raccolta tutta l’analisi e lo studio riguardante la creazione e il popolamento di un modello dimensionale nel quale sono confluiti dati raccolti in oltre 8000 pubblicazioni nel campo della fisica delle alte energie. Una delle sfide più impegnative di questo lavoro è stata quella di sfruttare le sorgenti di dati esistenti per rendere disponibili le informazioni in una nuova forma, più strutturata e facile da interrogare. Una volta acquisiti e caricati nel sistema di data warehouse, su questi dati, sarà possibile applicare le più moderne tecnologie in termini di data mining e data retrieval in grado, chissà, di rivelare una serie di \textbf{risultati fisici rimasti da sempre nascosti all’uomo}. 

\section{HepData}

\subsection{High Energy Physics}
La fisica delle alte energie (HEP) è una branca sperimentale e teorica della fisica moderna che studia i costituenti e le interazioni fondamentali della materia e della radiazione. L'obiettivo dell'HEP è quello di capire come l'universo funziona al livello delle sue componenti elementari.

\subsubsection{Esperimenti HEP}
Nel corso degli anni in questo campo della fisica sono stati collezionati numerosi risultati, uno tra i più noti al grande pubblico è sicuramente quello avvenuto nel 2012 che ha rilevato per la prima volta il \emph{bosone di Higgs} \cite{higs_boson}. La scoperta è stata realizzata sfruttando il più grande e potente acceleratore di particelle al mondo chiamato \emph{Large Hadron Collider} (LHC) che è l'ultima aggiunta al complesso di acceleratori del CERN di Ginevra\footnote{\url{https://home.cern}} \cite{about_cern}. Questa scoperta però non è altro che una delle ultime, infatti già negli anni precedenti moltissimi esperimenti sono comunque stati realizzati utilizzando altri acceleratori come il Tevatron del Fermi National Accelerator Laboratory a Batavia negli Stati Uniti d'America \cite{about_tevatron} o HERA ad Amburgo \cite{about_hera}.

\subsubsection{Dati degli esperimenti}
La realizzazione di tutti questi esperimenti ha portato alla necessità di avere uno strumento software in grado di raccogliere e memorizzare tutti i dati prodotti. L'\emph{\textbf{H}igh \textbf{E}nergy \textbf{P}hysics \textbf{Data}base} è stato sviluppato presso la \emph{Durham University} nel nord dell'Inghilterra nel corso degli ultimi quattro decenni con l'obiettivo di essere il principale punto di accesso e raccolta di tutti i risultati relativi ad esperimenti della fisica delle particelle. Attualmente contiene i dati di alcune migliaia di pubblicazioni, che includono anche quelle relative all'LHC. Il progetto HepData nell'ultimo decennio è stato oggetto di due grandi riprogettazioni. La prima, avvenuta una decina di anni fa, ha riguardato il database che è stato migrato da gerarchico a relazionale (MySQL) \cite{first_hepdata_upgrade}. La seconda, avvenuta quasi tre anni fa, ha riguardato tutto il progetto e ha comportato la migrazione verso l'utilizzo di tecnologie moderne come Python, PostgreSQL e Elasticsearch \cite{about_hepdata_2}.

\subsection{HepData: \emph{Il vecchio portale}}
Il portale \emph{online}: \url{http://durpdg.dur.ac.uk/HEPDATA}, mostrato in Figura \ref{durham_home} è ospitato su una singola macchina presso l'\emph{Institute for Particle Physics Phenomenology} (IPPP). Esso offre la possibilità, ad un \textbf{utente esperto}, di accedere a quei dati attraverso un'interfaccia web dalla quale si possono raggiungere le informazioni ricercate attraverso \textbf{query particolari}. 
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{img/durham_home.png}
	\caption{Homepage del vecchio sito di hepdata}
	\label{durham_home}
\end{figure}
Come già accennato questa soluzione è stata realizzata utilizzando un database MySQL per il salvataggio dei dati e un'interfaccia web basata su Java.

\subsection{HEPData: \emph{Il nuovo portale}}
\label{new_hepdata_portal}
Il nuovo portale di HEPData (notare la differenza nelle maiuscole nel nome), presentato il 25 Aprile 2016 al CERN, offre un modo innovativo di accedere a vecchie e nuove pubblicazioni ponendosi come il nuovo portale per il caricamento di tutti i dati dei nuovi esperimenti relativi alla fisica delle alte energie. Il sito Web è supportato da un \emph{cluster} di macchine messe a disposizione da \emph{CERN OpenStack}\footnote{\url{https://cernvm.cern.ch/portal/openstack}} che offrono parecchi vantaggi e caratteristiche migliori rispetto al vecchio portale. In Figura \ref{hepdata_home} la homepage del nuovo sito che, con la sua innovativa interfaccia grafica, mette a disposizione la consueta maschera per effettuare ricerche.
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.2]{img/hepdata_home.png}
	\caption{Homepage di hepdata.net}
	\label{hepdata_home}
\end{figure}

\subsubsection{La migrazione}
Come è già stato detto, il vecchio HepData salvava tutte le sue informazioni in un database relazionale (MySQL). Quando si doveva aggiungere un nuovo record, tutti i dati necessari (misurazioni e metadati) andavano trasformati in un file testuale correttamente formattato, questo file veniva poi interpretato da uno script Perl che inseriva automaticamente le informazioni nel database.

Nel nuovo sito di HEPData è stato deciso di salvare le varie pubblicazioni direttamente come file di testo invece che in un database relazionale, in modo tale che solo i metadati (e \textbf{non le misurazioni}) risultano essere ricercabili.
Al posto del vecchio formato di testo \emph{ad-hoc} è stato definito un nuovo standard utilizzando YAML\footnote{\url{http://yaml.org/}} che è un'estensione di JSON\footnote{\url{https://json.org/}} ma molto più \emph{human-readable}. Tutti i dati presenti nel vecchio HepData sono stati esportati nel nuovo formato YAML per la migrazione verso il nuovo sistema.

Questa ultima soluzione è sviluppata in maniera predominante in Python e Javascript come \emph{overlay} del \emph{framework} Invenio v3 \footnote{\url{http://inveniosoftware.org/}} \cite{about_hepdata_2}.

\subsubsection{Ricercabilità}
HEPData utilizza un database PostgreSQL (che possiede un nuovo modello di archiviazione e accesso ai dati rispetto al precedente MySQL), indicizzato con Elasticsearch\footnote{\url{https://www.elastic.co/}} per permettere un modo veloce e potente per effettuare ricerche su tutti i metadati. Tutti i contenuti su HEPData sono semanticamente arricchiti utilizzando il dizionario Schema.org \footnote{\url{https://schema.org/}}.

\subsubsection{Accessibilità}
Il nuovo portale risulta comodamente accessibile sia ad una persona che ad applicazioni software, risultando così pienamente conforme al \emph{5 Star Open Data} \footnote{\url{https://5stardata.info/en/}} \cite{about_hepdata_2}. 

\section{MineHep}
MineHEP è un nuovo modo di approcciarsi all'analisi dell'intero set di dati che sono stati pubblicati negli ultimi cinquanta anni attraverso la \emph{community} di HEP. Questi dati rappresentano un'enorme e quasi inesplorata miniera d'oro dove nuova fisica può facilmente passare inosservata. Infatti, le informazioni su HepData/HEPData sono organizzate in maniera non correlata e non omogenea. Gli utenti di HepData/HEPData possono utilizzare un motore di ricerca per identificare esperimenti simili basati su parole chiave contenute nei vari articoli, \textbf{ma la possibilità di comparare e mettere in relazione i dati da differenti esperimenti non è consentita} ed è lasciata completamente a carico dell'utente. Quindi, sebbene le informazioni siano tutte nello stesso posto, l'assenza di una rigida struttura dati e di una chiara relazione tra i dati prodotti da differenti lavori impedisce l'applicazione di procedure di \emph{data mining} e analisi statistica dei dati. Così, i trend statistici come le relazioni o le similarità tra gli esperimenti, specialmente quando se ne devono considerare parecchi, \textbf{rimangono nascosti}. 
\newline
MineHep fornirà uno strumento per estrarre le informazioni rilevanti in maniera coerente da questo enorme \emph{dataset}, utilizzando tecniche avanzate di \emph{data mining} e di analisi, le quali non hanno precedenti nel contesto \emph{HEP}.
\newline
Nel dettaglio MineHep punta a:
\begin{itemize}
	\item Organizzare i dati contenuti in HepData in modo coerente con il data warehouse, che permette l'indicizzazione in diverse dimensioni.
	\item Permettere query complesse sul data warehouse per estrarre informazioni rilevanti.
	\item Costruire un modello statistico che rappresenti i dati di tutte le analisi.
	\item Dato un nuovo modello fisico, produrre, in maniera automatica, il modello delle predizioni delle quantità misurate nelle pubblicazioni selezionate.
\end{itemize}
Questi obiettivi saranno raggiunti con la realizzazione di strumenti software equipaggiati con una interfaccia Web che permetterà a un qualunque nuovo modello fisico di essere testato in modo coerente con l'insieme totale dei dati raccolti negli anni. Seguendo l'approccio del data warehousing, lo strumento sarà costituito dei seguenti componenti:
\begin{itemize}
	\item Unità di memorizzazione dei dati organizzata con una struttura \emph{OLAP} (\emph{On-Line Analytical Processing}), popolata periodicamente a partire da HepData.
	\item Un'interfaccia per l'esecuzione di query per la ricerca di dati memorizzati nell'unità di memorizzazione.
	\item Un \emph{tool} per la modellazione statistica basato sul software \emph{ROOT} \cite{root_software} (usato comunemente negli esperimenti HEP) per organizzare i dati degli esperimenti estratti dal database in un modello statistico che poi è usato nel contesto del metodo \emph{Maximum Likelihood}.
	\item Uno strumento per la creazione di predizioni, batato su \emph{RIVET} \cite{rivet_software}.
\end{itemize}
\textbf{MineHep è un cambiamento nel paradigma della ricerca di nuova fisica}. Mentre le odierne analisi nel campo HEP sono pensate per la ricerca di uno specifico tipo di nuova fisica, MineHep approfitta della disponibilità dei dati presenti e futuri nel database HepData per testare la fisica in modo generale, e permettere di reinterpretare i vecchi dati in un contesto di nuovi modelli, rendendo così i dati raccolti più duraturi e utili nel corso del tempo.

Il lavoro che sarà presentato nel corso di questa tesi è incentrato sullo sviluppo della prima e della seconda componente ovvero: memorizzazione in forma \emph{OLAP} e interfaccia per ricerca dati. 

\section{Panoramica del lavoro svolto}

Il lavoro contenuto in questa tesi verte principalmente sull'analisi e la realizzazione di un sistema di data warehousing in grado di raccogliere i dati di esperimenti di fisica delle alte energie. La tesi è scritta utilizzando un approccio al problema del tipo \emph{bottom-up}. Si partirà con una introduzione sul mondo del DW studiando la teoria e le tecniche base. Dopo aver acquisito un minimo di conoscenze arriveremo a trattare il caso specifico, giungendo poi alla definizione di una possibile soluzione.

Nella prima parte studieremo le nozioni di DW illustrate in uno dei libri più affermati del settore \cite{datawarehouse_toolkit}, sfruttando i concetti esposti in quest'opera per il nostro caso d'interesse. Dimostreremo che le tecniche, nate per l'analisi dei processi di business aziendali, possono essere utilizzate anche in altri ambiti dove esiste la necessità di collezionare grosse moli di dati per poi effettuare interrogazioni che siano il più possibile semplici e veloci. 

Una volta presa confidenza con concetti come quelli di schema a stella, dimensione e fatti proseguiremo nello studio delle varie possibilità disponibili per l'acquisizione dei dati degli esperimenti. Ci concentreremo in particolare su due sorgenti che sono una l'evoluzione dell'altra. La più datata è quella che utilizza un \emph{Relational Database Management System} (RDBMS) mentre la più recente, invece, è completamente diversa e fornisce direttamente una serie di API\footnote{Application Programming Interface} \emph{online} per l'accesso a tutti i dati delle pubblicazioni.

Analizzati pregi e difetti delle soluzioni a disposizione selezioneremo quella che più sarà adatta al nostro scopo motivandone attentamente la scelta. Con questa sorgente di dati scelta andremo a definire il nostro schema a stella ideale e modelleremo le informazioni originali così da poter essere inserite all'interno della nuova struttura. Questa fase è stata la più articolata di tutto il progetto e ha richiesto una fase di analisi molto lunga e lo sviluppo di \emph{script} complessi per adattare i vecchi dati al nuovo schema. Per assicurare coerenza e consistenza dei dati è anche stata prevista una fase di collaudo nella quale sono stati scritti alcuni test per valutare l'affidabilità del lavoro svolto.

Nel finale osserveremo la soluzione messa in piedi ed esporremo i limiti che essa si porta dietro cercando di fornire alcune possibili soluzioni alternative.

\section{Il Team}
\label{team_section}
Il team multidisciplinare che ha lavorato attivamente all'analisi e alla realizzazione di questo lavoro è composto, oltre a me, da 5 persone:
\begin{itemize}
	\item \textbf{Andrea Ceccarelli}: relatore della tesi; mi ha introdotto al progetto e seguito dalle prime fasi fino alla fine.
	\item \textbf{Lorenzo Escriva}: si è occupato della parte di presentazione dei dati che riguarda la visualizzazione delle informazioni nello schema a stella tramite \emph{dashboards}.
	\item \textbf{Laura Redapi}: ha partecipato alla fase di analisi e test sperimentando i limiti e le potenzialità delle prime versioni dello schema a stella.
	\item \textbf{Piergiulio Lenzi}: ha partecipato alla fase di analisi ed ha mantenuto i contatti con l'Università di Durham, grazie ai quali siamo entrati in possesso del \emph{dump} MySQL contenente i dati delle pubblicazioni.
	\item \textbf{Maria Vittoria Garzelli}: si è unita al team nelle ultime fasi di test. Ha cominciato a lavorare sullo schema a stella sia direttamente che indirettamente attraverso le dashboard e ha revisionato la scrittura di questa tesi.
\end{itemize}

\section{Struttura della tesi}
\begin{itemize}
	\item \textbf{Capitolo 1}: introduzione della tesi.
	\item \textbf{Capitolo 2}: introdurremo la teoria alla base del lavoro della tesi nonch\'e gli strumenti software utilizzati.
	\item \textbf{Capitolo 3}: analizzeremo le caratteristiche delle sorgenti dati a nostra disposizione valutandone pregi e difetti. Sulla base di ciò opereremo una scelta a favore di una piuttosto che di un'altra. Con la sorgente definita andremo poi ad organizzare il lavoro da svolgere.
	\item \textbf{Capitolo 4}: progetteremo lo schema a stella sulla base delle necessità studiando attentamente la struttura dei dati originali. Spiegheremo come si sia giunti alla definizione delle tabelle delle dimensioni e della tabella dei fatti.
	\item \textbf{Capitolo 5}: cominceremo a spiegare come sia stato possibile collegare i fatti alle dimensioni sfruttando l'organizzazione dei dati originali. Alla fine di questo capitolo il nostro nuovo schema a stella sarà pronto e verrà collaudato attraverso una serie di test.
	\item \textbf{Capitolo 6}: analizzeremo il lavoro svolto evidenziandone le criticità e cercando di indicare possibili modi per risolverle.
	\item \textbf{Capitolo 7}: saranno illustrati i passaggi (necessari a chi fosse interessato) per ottenere una copia perfettamente funzionante del nuovo sistema di DW sul proprio computer partendo dalla sorgente dati originale.
\end{itemize}