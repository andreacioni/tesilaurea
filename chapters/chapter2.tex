\chapter{Fondamenti}
In questo capitolo si introducono i concetti alla base di questo lavoro focalizzando l'attenzione sul tema centrale di nostro interesse che è il data warehouse. Quindi si analizzeranno le tecniche, ormai consolidate, che sono necessarie ogni qual volta ci sia la necessità di approcciarsi a questa tecnologia.

\section{Data Warehouse}
\label{data_warehouse}
\begin{center}
     {\large "Il \emph{Data Warehouse} (DW) è il sistema usato per la reportistica e l'analisi dei dati ed è considerato il componente di base della \emph{Business Intelligence} (BI) ."\cite{datawarehouse_def}}
\end{center}

Già da questa prima definizione si intuisce come questa tecnologia nasca in un settore, quello \textbf{commerciale}, che a prima vista potrebbe sembrare non avere niente a che vedere con quello della fisica. I paragrafi successivi dovrebbero aiutare il lettore a familiarizzare con i concetti base di DW che saranno utilizzati nei capitoli che seguiranno nonch\'e a convincerlo che questa tecnologia possa essere utilizzata nell'ambito del progetto \emph{MineHep}.

\subsection{Obiettivi del DW}
Prima di scendere nei dettagli di quello che è il modello dimensionale, può aiutare concentrare la propria attenzione sugli obiettivi del DW e della BI. I più importanti per il nostro progetto sono i seguenti \cite{datawarehouse_toolkit}: 
\begin{itemize}
    \item \textbf{Il sistema DW/BI deve permettere di accedere facilmente alle informazioni}. Il contenuto delle informazioni deve essere comprensibile, intuitivo e ovvio per l'utente finale.
    \item \textbf{Il sistema DW/BI deve essere facilmente modificabile}. Le esigenze degli utenti, le condizioni e le tecnologie sono tutte soggette al cambiamento. Il sistema DW/BI deve essere predisposto per gestire questi inevitabili cambiamenti senza la necessità di dover riprogettare tutto e senza impattare i dati o le applicazioni esistenti.
    \item \textbf{Il sistema DW/BI deve presentare le informazioni in maniera tempestiva}.
\end{itemize}
\subsection{Modello dimensionale}
Adesso che conosciamo gli obiettivi del DW/BI, consideriamo le basi del \textbf{modello dimensionale}. Il modello dimensionale è ampiamente accettato come la \textbf{tecnica preferita} per presentare dati analitici perch\'e risponde già a due requisiti:
\begin{itemize}
    \item Fornire i dati in maniera chiara all'utente finale
    \item Fornire la possibilità di accedere velocemente ai dati
\end{itemize}
Il modello dimensionale è una tecnica di lunga data per ottenere database semplici. Per più di cinque decadi, le aziende informatiche, i consulenti e gli utenti hanno utilizzato semplici strutture dimensionali per servire le esigenze delle persone. La semplicità è critica perch\'e assicura che gli utenti possano facilmente capire i dati, ma anche per permettere ai software di navigare e consegnare i risultati velocemente ed efficientemente.
\paragraph{Modello dimensionale e Modello ER}
Sebbene il modello dimensionale sia spesso istanziato in un database relazionale, ci sono delle piccole differenze rispetto al modello della $3^\circ$ Forma Normale ($3NF$) che punta a rimuovere la ridondanza. Le strutture normalizzate dividono i dati in più entità discrete, ciascuna delle quali diventa una tabella relazionale. L'industria si riferisce spesso al modello $3NF$ come al Modello Entità-Relazione (ER). I diagrammi ER (ERD) sono disegnati in modo da comunicare le relazioni tra le tabelle. Entrambi $3NF$ e modello dimensionale possono essere rappresentati attraverso un ERD, perch\'e entrambi consistono nel \emph{join} di tabelle relazionali; la differenza chiave tra i due è \textbf{il grado di normalizzazione}. 

Le strutture normalizzate in forma $3NF$ sono molto utili perch\'e nella fase di inserimento/aggiornamento vanno a toccare il database in un solo ed unico punto. \textbf{I modelli normalizzati, però, sono troppo complicati per le query BI}. Gli utenti non riescono a capire, navigare o ricordare i modelli normalizzati troppo complessi. Allo stesso modo, molti RDBMS non riescono ad eseguire efficientemente query in modello normalizzato; l'impossibilità di prevedere il grado di complessità delle query che un utente può avere in mente di effettuare rischia di sopraffare l'ottimizzatore di qualunque database andando incontro a effetti disastrosi.

Fortunatamente, il modello dimensionale risolve questi problemi di eccessiva complessità degli schemi.
\subsection{Star Schema e OLAP}
Un modello dimensionale può essere implementato sia attraverso l'uso di un database relazionale, e in questo caso si parla di \emph{Star Schema}, oppure attraverso un database multidimensionale che viene chiamato cubo \emph{Online Analytic Processing} (OLAP) come mostrato in Figura \ref{star_vs_olap_cubes}. 
\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{img/star_vs_olap_cubes.png}
	\caption{Star Schema vs. OLAP cubes}
	\label{star_vs_olap_cubes}
\end{figure}
Gli \emph{Star Schema} consistono in \textbf{tabelle dei fatti} collegate a \textbf{tabelle delle dimensioni} attraverso chiavi primarie/esterne. Gli OLAP \emph{cubes} possono essere equivalenti nel contenuto a (ma spesso derivati da) uno \emph{Star Schema} relazionale. I cubi OLAP contengono attributi dimensionali e fatti, a cui si accede attraverso linguaggi con capacità analitiche maggiori rispetto a SQL, come XMLA\footnote{\url{http://bit.ly/2W4ul1T}}\cite{star_schema_vs_olap_cubes}.

Se un sistema DW/BI possiede uno schema a stella o un cubo OLAP allora si basa sul modello dimensionale. Entrambi i modelli a stella e cubo hanno in comune il design e le dimensioni, anche se l'implementazione reale differisce.

Quando un dato è caricato dentro un cubo OLAP, questo è salvato e indicizzato utilizzando formati e tecniche che sono state pensate per dati dimensionali. Il motore OLAP, attraverso l'ottimizzazione delle \emph{performance}, l'uso di tabelle riassuntive precalcolate, l'indicizzazione e altre ottimizzazioni permette di effettuare query più performanti. Gli utenti possono andare più nel dettaglio aggiungendo o rimuovendo attributi senza modificare le query. I cubi OLAP mettono a disposizione funzioni analitiche più robuste che vanno ben oltre quelle disponibili in SQL. Il difetto è che queste caratteristiche si pagano in termini di performance, specialmente con insiemi di dati molto grandi \cite{datawarehouse_toolkit}.

\subsection{Architettura}
Cominciamo adesso ad entrare nei dettagli del sistema DW/BI e del modello dimensionale investigando i componenti di un ambiente DW/BI basato sull'\textbf{architettura Kimball} \cite{datawarehouse_toolkit}. Questo tipo di architettura prende il nome da \emph{Ralph Kimball} che è stato uno dei padri del DW/BI e ha gettato le basi di questa disciplina che abbiamo discusso fino ad ora.

Come illustrato in Figura \ref{kimball_architecture} ci sono \textbf{quattro componenti distinte} da considerare in un ambiente DW/BI:
\begin{itemize}
    \item \emph{Operational Source Systems}
    \item \emph{ETL Systems}
    \item \emph{Data Presentation Area}
    \item \emph{Business Intelligence Applications}
\end{itemize}
\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{img/kimball_architecture.png}
	\caption{Elementi centrali dell'architettura Kimball}
	\label{kimball_architecture}
\end{figure}

\subsubsection{Operational Source Systems}
Sono i sistemi che registrano i vari eventi sotto forma di dati e costituiscono la sorgente di informazioni utilizzata dal sistema DW/BI. Bisogna pensare a questi sistemi come esterni al data warehouse perch\'e presumibilmente si potrebbe avere poco, o nessun, controllo sui contenuti e sul formato dei dati di questi sistemi operazionali. Le priorità di questi sistemi sorgente sono \emph{performance} e disponibilità. Su questi sistemi non c'è necessità di richiedere informazioni in maniera ampia e imprevedibile come invece avviene nei sistemi DW/BI. I sistemi sorgente mantengono una storia limitata; un buon data warehouse può alleggerire i sistemi operazionali dalla responsabilità di rappresentare il passato.

\subsubsection{Sistema ETL}
I sistemi ETL, acronimo di \emph{Extract, Transformation and Load}, del DW/BI rappresentano tutto ciò che si trova tra i sistemi sorgente e il livello di presentazione dei dati.

\paragraph{Extraction} L'estrazione è il primo passaggio nel processo di importazione dei dati nell'ambiente di data warehouse. Estrarre significa leggere e capire la sorgente dati e copiare i dati necessari nel sistema ETL per le manipolazioni future. A questo punto il dato appartiene al sistema DW/BI. 

\paragraph{Transformation} Una volta che il dato è estratto dal sistema ETL, ci sono innumerevoli possibilità di trasformare i dati, come ripulire e normalizzare le informazioni estratte (correggere errori grammaticali, gestire elementi mancanti ed eseguire il \emph{parsing} in formati standard), combinandole con i dati di altre sorgenti dati, e rimuovendo i duplicati. Il sistema ETL aggiunge valore ai dati attraverso queste fasi di ripulitura e conformazione aggiustandoli e arricchendoli.  

\paragraph{Load} Lo step finale del processo di ETL è lo strutturare e caricare i dati nel modello dimensionale di riferimento, perch\'e la prima missione dei sistemi ETL è quella di adattare i dati ad una struttura dati già esistente e non modificabile. 

\subsubsection{Presentation Area}
La \emph{presentation area} del DW/BI è dove i dati sono organizzati, salvati e resi disponibili attraverso un \textbf{modello dimensionale} (schema a stella o cubo OLAP) per richieste degli utenti, per le reportistiche e per altre applicazioni analitiche riferite alla BI. I dati contenuti nella \emph{presentation area} devono essere \textbf{atomici e dettagliati} per sopperire alle più svariate richieste che gli utenti possono fare al sistema di DW/BI. Sebbene la \emph{presentation area} possa contenere anche dei dati aggregati per migliorare le sue performance, non è sufficiente mettere a disposizione tali dati senza avere anche a disposizione le informazioni originali.

Mentre delle prime due parti me ne sono occupato principalmente io, questa ultima parte è stata seguita principalmente da Lorenzo (\ref{team_section}) il quale ha studiato le tecnologie disponibili in questo settore e si è appoggiato al lavoro frutto di questa tesi per testare i propri risultati.

\section{Data Mining e Information Retrieval}

\subsection{Data Mining}
Il \emph{data mining} (DM) è un processo di estrazione di conoscenza da banche dati di grandi dimensioni, che avviene tramite l'applicazione di algoritmi che individuano le associazioni "nascoste" tra i dati e le rendono visibili. In altre parole è il processo di esplorazione e analisi, che ha una natura automatica o semi-automatica, al fine di scoprire dei modelli (\emph{patterns}) e regole significative dai dati. Questo processo consiste nell'estrazione complessa di informazioni implicite, precedentemente sconosciute e potenzialmente utili all'interno di grandi archivi di dati. Un pattern indica una struttura, che in generale è una rappresentazione sintetica dei dati. Si parte dall’assunto che ci sia più conoscenza nascosta nei dati di quella che si mostra in superficie e si usano tecniche statistiche, simboliche, sub-simboliche e di visualizzazione. Le tecniche utilizzabili sono molteplici e, di conseguenza, così sono anche gli appositi algoritmi di apprendimento che vengono implementati. La scelta dell’algoritmo dipende principalmente dall’obiettivo che si vuole raggiungere e dal tipo di dati da analizzare \cite{datamining_ir_thesis}.


\subsection{Information Retrieval}
L’\emph{information retrieval} (IR) è l'insieme delle tecniche utilizzate a trovare informazioni che soddisfino le esigenze informative degli utenti. Concretamente, si concentra sulla struttura, analisi, rappresentazione, memorizzazione, organizzazione, distribuzione e reperimento dell’informazione. Per cui lo scopo di un sistema di IR è di trovare informazioni che potrebbero essere utili e/o rilevanti per l’utente finale. L’enfasi non è sulla ricerca dei dati ma sulla ricerca delle informazioni. Un sistema di information retrieval (IRS) è realizzato per svolgere in modo automatico i compiti di reperimento delle informazioni. Le componenti dell’IRS rappresentano le strutture dei dati e, mediante gli algoritmi, automatizza i processi di indicizzazione dei dati e di reperimento delle informazioni. L’IRS, a seguito di un'interrogazione, calcola la rilevanza di ciascun documento indicizzato rispetto alla stessa. L'IRS svolge anche la collezione dei documenti di qualsivoglia dimensione, assicurando la descrizione del contenuto informativo e reperimento veloce dei documenti che rappresentano informazioni rilevanti per rispondere alle esigenze informative. Per il fatto della convenienza e dell’abbondanza delle informazioni disponibili sul web, l’IR nella ricerca sul web ha assunto un ruolo dominante. In information retrieval, si utilizza la parola documento per indicare un contenitore persistente di dati e identificabile in modo univoco. Alcuni esempi di documento possono essere libri, capitoli di un libro, articoli scientifici, quelli quotidiani o periodici, video, discorsi, immagini ecc. Un documento ha il ruolo di ”documentare”, poich\'e le informazioni rappresentate da quei dati contribuiscono alla formazione della conoscenza di una persona. Pertanto in IR, una collezione  di  documenti è l’insieme di documenti da rappresentare, descrivere, memorizzare e da gestire in modo automatico \cite{datamining_ir_thesis}. 


\section{Tools}

\subsection{MySQL}
MySQL è un RDBMS open source e libero che rappresenta una delle tecnologie più note e diffuse nel mondo dell’IT. MySQL nacque nel 1996 per opera dell’azienda svedese Tcx, basato su un DBMS relazionale preesistente, chiamato mSQL. Il progetto venne distribuito in modalità open source per favorirne la crescita.

Dal 1996 ad oggi, MySQL si è affermato molto velocemente prestando le sue capacità a moltissimi software e siti Internet. I motivi di tale successo risiedono nella sua capacità di mantenere fede agli impegni presi sin dall’inizio:
\begin{itemize}
    \item alta efficienza nonostante le moli di dati gestite;
    \item integrazione di tutte le funzionalità che offrono i migliori DBMS: indici, \emph{trigger} e \emph{stored procedure};
    \item altissima capacità di integrazione con i principali linguaggi di programmazione, ambienti di sviluppo e suite di programmi da ufficio.
\end{itemize}

\subsection{Docker}
Docker è un progetto open-source che automatizza il deployment (consegna o rilascio al cliente, con relativa installazione e messa in funzione o esercizio, di una applicazione o di un sistema software tipicamente all'interno di un sistema informatico aziendale) di applicazioni all'interno di contenitori software, fornendo un'astrazione aggiuntiva grazie alla virtualizzazione a livello di sistema operativo Linux. Docker utilizza le funzionalità di isolamento delle risorse del kernel Linux come ad esempio cgroups e namespaces per consentire a \textbf{container} indipendenti di coesistere sulla stessa istanza di Linux, evitando l'installazione e la manutenzione di una macchina virtuale
\cite{why_docker}.
Concetto centrale quando si parla di Docker è proprio quello del \emph{container}, questi non sono altro che delle unità software che impacchettano il codice e tutte le sue dipendenze in modo che le applicazioni possano partire velocemente e in maniera affidabile anche in ambienti differenti. Un "immagine" Docker di un \emph{container} non è altro che un piccolo componente \emph{standalone} eseguibile che include tutto il necessario per essere avviato (codice, \emph{runtime}, librerie e settaggi) \cite{what_a_docker_container}.
\subsection{Python}
Python è un linguaggio di programmazione dinamico orientato agli oggetti utilizzabile per molti tipi di sviluppo software. Offre un forte supporto all'integrazione con altri linguaggi e programmi, è fornito di una estesa libreria standard e può essere imparato velocemente. Molti programmatori Python possono confermare un sostanziale aumento di produttività e ritengono che il linguaggio incoraggi allo sviluppo di codice di qualità e manutenibilità superiori \cite{python_intro}. 
