\chapter{Creazione e Popolamento Star Schema}
Adesso che sappiamo dove vogliamo arrivare possiamo concentrarci sulla parte relativa a come ci si arriva. In questo capitolo introdurremo le query necessarie a migrare i dati da uno schema all'altro, creando le dimensioni, le tabelle temporanee di collegamento (\emph{link tables}) e infine la tabella dei fatti. In questa sezione quando parleremo di tabelle \emph{temporanee} ci riferiremo a tabelle che, una volta finita la fase di caricamento dello schema a stella, potranno essere eliminate senza problemi.

\section{Preparazione alla migrazione}
\label{examine_link_tables_chapther}
Nel capitolo \ref{examine_dimension_chapther} abbiamo mostrato le query necessarie per la creazione delle dimensioni; crearle è stato abbastanza semplice, in sostanza è bastato prendere tutti i possibili valori associabili ad un punto, assegnargli un ID e inserirli in una nuova tabella.
Il popolamento della tabella dei fatti purtroppo non è una cosa semplice allo stesso modo. Per arrivare alla sua forma finale è stato necessario creare delle tabelle temporanee di collegamento che esponessero i dati in una certa struttura che adesso andremo ad analizzare.

\subsection{Tabelle temporanee di collegamento}
Utilizziamo sempre l'esempio delle \emph{keywords} per studiare la creazione di una tabella temporanea di collegamento che servirà poi per costruire la tabella dei fatti. La procedura che andremo a spiegare per la KeywordsDim è sostanzialmente uguale per ricavare tutte le tabelle temporanee di collegamento rispetto a qualunque altra dimensione.
Partiamo dando uno sguardo alle istruzioni che generano queste tabelle particolari:
\begin{lstlisting}[language=sql]
    -- Initialize variables used below
    set @row_number := -1, @vid := -1;
    

    drop table if exists temp_RowPerGroup_Keywords;
    create table temp_RowPerGroup_Keywords (primary key (VALUE_ID, ID))
        select	@row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
                @vid := VALUE_ID as VALUE_ID,
                ID
        from (
            select distinct VALUE_ID, ID
            from DsKeywords dsk
            inner join YAxes yax on yax._dataset_DATASET_ID = dsk.DATASET_ID
            inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
            inner join minehep.KeywordsDim dik on dik.Name <=> dsk.Keyword
            order by VALUE_ID
        ) tmp;
    
    drop table if exists temp_Link_Keywords;
    create table temp_Link_Keywords (primary key (VALUE_ID))
            select
            VALUE_ID,
            max(case when ROW_NUM=1  then dsk.ID else null end) as KEYWORD_ID_1,
            max(case when ROW_NUM=2  then dsk.ID else null end) as KEYWORD_ID_2,
            max(case when ROW_NUM=3  then dsk.ID else null end) as KEYWORD_ID_3,
            max(case when ROW_NUM=4  then dsk.ID else null end) as KEYWORD_ID_4,
            max(case when ROW_NUM=5  then dsk.ID else null end) as KEYWORD_ID_5,
            max(case when ROW_NUM=6  then dsk.ID else null end) as KEYWORD_ID_6,
            max(case when ROW_NUM=7  then dsk.ID else null end) as KEYWORD_ID_7,
            max(case when ROW_NUM=8  then dsk.ID else null end) as KEYWORD_ID_8,
            max(case when ROW_NUM=9  then dsk.ID else null end) as KEYWORD_ID_9,
            max(case when ROW_NUM=10 then dsk.ID else null end) as KEYWORD_ID_10,
            max(case when ROW_NUM=11 then dsk.ID else null end) as KEYWORD_ID_11,
            max(case when ROW_NUM=12 then dsk.ID else null end) as KEYWORD_ID_12,
            max(case when ROW_NUM=13 then dsk.ID else null end) as KEYWORD_ID_13,
            max(case when ROW_NUM=14 then dsk.ID else null end) as KEYWORD_ID_14
        from temp_RowPerGroup_Keywords dsk
        group by VALUE_ID;
\end{lstlisting}
tralasciando la parte di definizione di variabili locali, la cosa che salta immediatamente all'occhio è che per creare la tabella temporanea di collegamento \emph{temp\_Link\_Keywords} ci si appoggia ad un' altra tabella temporanea di nome \emph{temp\_RowPerGroup\_Keywords}, è proprio quest'ultima che è di maggiore interesse per il nostro studio. Si è deciso di mantenere le due query separate per una questione di praticità e per favorire il \emph{debug} degli errori che sorgevano durante lo sviluppo.

\subsection{Row Per Group Table}
La query che crea questa tabella è a sua volta composta da una \emph{subquery}:
\begin{lstlisting}[language=sql]
    select distinct VALUE_ID, ID
    from DsKeywords dsk
    inner join YAxes yax on yax._dataset_DATASET_ID = dsk.DATASET_ID
    inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
    inner join minehep.KeywordsDim dik on dik.Name <=> dsk.Keyword
    order by VALUE_ID
\end{lstlisting}
essa costituisce il \emph{join} tra le 4 tabelle: \emph{DsKeywords}, \emph{YAxes}, \emph{Points} e \emph{KeywordsDim} e produce come risultato una tabella nella quale si hanno 2 colonne:
\begin{itemize}
    \item VALUE\_ID: che riporta l'ID dei singoli valori dell'asse Y presi dalla tabella Points dallo schema HepData originale. Questi poi, trasformati in UUID, diventeranno i vari $Yx$ a cui si faceva riferimento in \ref{relation_cardinality}. Mantenere questa colonna è essenziale per poter costruire una relazione tra vecchi ($VIDx$) e nuovi ($Yx$) ID e poter quindi ottenere la tanto agognata tabella dei fatti.
    \item ID: che riporta gli ID associati al record relativo della dimensione \emph{KeywordsDim}; questi ID saranno posizionati nelle righe dei vari record della \emph{facts table}.
\end{itemize}
La tabella avrà una forma del tipo mostrato in tabella:
\begin{center}
    \begin{tabular}{|l|l|}
        \hline
        VALUE\_ID & ID \\
        \hline
        VID1 & KEYWID4 \\
        VID1 & KEYWID2 \\
        VID1 & KEYWID1 \\
        VID3 & KEYWID1 \\
        VID3 & KEYWID3 \\
        VID4 & KEYWID2 \\
        \hline
    \end{tabular}
\end{center}
Eseguendo la \emph{select}
\begin{lstlisting}[language=sql]
    ...
    select	@row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
            @vid := VALUE_ID as VALUE_ID,
            ID
    ...
\end{lstlisting}
da una tabella di quel tipo se ne ottiene una simile che conta una colonna in più nella quale appare un numero progressivo che si riporta a 1 ogni qual volta il \emph{VALUE\_ID} cambia di valore. perch\'e questa \emph{select} funzioni a dovere è \textbf{necessario} inizializzare le variabili locali \emph{row\_number}, \emph{vid} ed \textbf{effettuare l'ordinamento} secondo \emph{VALUE\_ID} come mostrato nella \emph{subquery} precedente.
L'esecuzione della \emph{select} sopra porta ad avere una tabella di questo tipo:
\begin{center}
    \begin{tabular}{|l|l|l|}
        \hline
        ROW\_NUM & VALUE\_ID & ID \\
        \hline
        1 & VID1 & KEYWID4 \\
        2 & VID1 & KEYWID2 \\
        3 & VID1 & KEYWID1 \\
        1 & VID3 & KEYWID1 \\
        2 & VID3 & KEYWID3 \\
        1 & VID4 & KEYWID2 \\
        \hline
    \end{tabular}
\end{center}

\subsection{Link Table}
Con una tabella siffatta siamo in grado, abbastanza semplicemente, di crearne un'altra che esponga i dati invece che in riga, in colonna attraverso il comando MySQL:
\begin{lstlisting}[language=sql]
    create table temp_Link_Keywords (primary key (VALUE_ID))
        select
        VALUE_ID,
        max(case when ROW_NUM=1  then dsk.ID else null end) as KEYWORD_ID_1,
        max(case when ROW_NUM=2  then dsk.ID else null end) as KEYWORD_ID_2,
        max(case when ROW_NUM=3  then dsk.ID else null end) as KEYWORD_ID_3,
        max(case when ROW_NUM=4  then dsk.ID else null end) as KEYWORD_ID_4,
        max(case when ROW_NUM=5  then dsk.ID else null end) as KEYWORD_ID_5,
        max(case when ROW_NUM=6  then dsk.ID else null end) as KEYWORD_ID_6,
        max(case when ROW_NUM=7  then dsk.ID else null end) as KEYWORD_ID_7,
        max(case when ROW_NUM=8  then dsk.ID else null end) as KEYWORD_ID_8,
        max(case when ROW_NUM=9  then dsk.ID else null end) as KEYWORD_ID_9,
        max(case when ROW_NUM=10 then dsk.ID else null end) as KEYWORD_ID_10,
        max(case when ROW_NUM=11 then dsk.ID else null end) as KEYWORD_ID_11,
        max(case when ROW_NUM=12 then dsk.ID else null end) as KEYWORD_ID_12,
        max(case when ROW_NUM=13 then dsk.ID else null end) as KEYWORD_ID_13,
        max(case when ROW_NUM=14 then dsk.ID else null end) as KEYWORD_ID_14
        from temp_RowPerGroup_Keywords dsk
        group by VALUE_ID;
\end{lstlisting}
Sapendo, dall'analisi precedentemente effettuata, che il numero massimo di \emph{keywords} per un valore dell'asse Y è 14 siamo in grado attraverso un piccolo "trucco" di portare i vari ID relativi ad un \emph{VALUE\_ID} tutti in una sola riga, ottenendo una tabella del tipo:
\begin{center}
    \begin{tabular}{|l|l|l|l|}
        \hline
        VALUE\_ID & KEYWORD\_ID\_1 & KEYWORD\_ID\_2 & KEYWORD\_ID\_3 \\
        \hline
        VID1 & KEYWID4 & KEYWID2 & KEYWID1  \\
        VID2 & KEYWID1 & KEYWID3 & NULL     \\
        VID3 & KEYWID2 & NULL    & NULL     \\
        \hline
    \end{tabular}
\end{center}
Infatti raggruppando per \emph{VALUE\_ID} ed eseguendo la funzione di aggregazione \emph{MAX} su un'espressione del tipo: \emph{case when ROW\_NUM=x then dsk.ID else null end}, se per un certo valore di Y esiste una riga con \emph{ROW\_NUM = x} allora si inserisce l'ID corrispondente in corrispondenza della colonna \emph{KEYWORD\_ID\_x}. Questa tecnica è nota nel mondo dei database e prende il nome di \emph{Pivoting} delle righe alle colonne \cite{pivoting_row_to_column_sql}.

Effettuando la stessa operazione per tutte le altre tabelle delle altre dimensioni ci troviamo in mano tutto il necessario per la creazione della tabella dei fatti.

\subsection{Facts Table}
Rimanendo sempre sul nostro esempio delle \emph{keywords} mostriamo ora un estratto dello script che ha generato l'intera tabella dei fatti (lo script completo ha quasi $400$ righe!):
\begin{lstlisting}[language=sql]
create table YAxisFacts (primary key(ID), unique(VALUE_ID))
    select 	UUID() ID, 
			lya.Header,
			pts.Value,
			pts.ValueLength,
			pts.Description,
			pts.Relation,
            lds.TableNumber,
            ...
			lks.KEYWORD_ID_1,
			lks.KEYWORD_ID_2,
			lks.KEYWORD_ID_3,
			lks.KEYWORD_ID_4,
			lks.KEYWORD_ID_5,
			lks.KEYWORD_ID_6,
			lks.KEYWORD_ID_7,
			lks.KEYWORD_ID_8,
			lks.KEYWORD_ID_9,
			lks.KEYWORD_ID_10,
			lks.KEYWORD_ID_11,
			lks.KEYWORD_ID_12,
			lks.KEYWORD_ID_13,
			lks.KEYWORD_ID_14
	from hepdata.Points pts
    inner join hepdata.temp_Link_YAxes lya 
        on lya.VALUE_ID = pts.VALUE_ID
    inner join hepdata.temp_Link_Datasets lds 
        on lds.VALUE_ID = pts.VALUE_ID
    left join hepdata.temp_Link_Papers lpa on lpa.VALUE_ID = pts.VALUE_ID
	...
    left join hepdata.temp_Link_Keywords lks 
        on lks.VALUE_ID = pts.VALUE_ID;
\end{lstlisting}
Le prime 7 colonne si riferiscono tutte alla grana del modello dimensionale, le più importanti si riferiscono:
\begin{itemize}
    \item all'intestazione dell'asse Y nel quale la misurazione si trova (\emph{Header})
    \item \textbf{al valore misurato sull'asse Y} (\emph{Value})
    \item \emph{dataset} nel quale la misurazione si trova; in \emph{join} con la tabella \emph{PaperDim} questo campo ci permette di risalire alla tabella dove si trova quella misurazione. Più avanti quando parleremo di test questa informazione sarà molto importante.
\end{itemize}
Scorrendo la lista dei vari campi della tabella incontriamo i 14 che abbiamo discusso nel precedente capitolo quando abbiamo parlato della creazione della \emph{temp\_Link\_Keywords} che appare in \emph{left join} con le tabelle \emph{Points}, \emph{temp\_Link\_YAxes} e \emph{temp\_Link\_Datasets} che a loro volta costituiscono \textbf{il cuore della nostra tabella dei fatti}. È importante la scelta di effettuare un \emph{left join} per questa tabella temporanea di collegamento per far si che anche i valori della Y a cui non sono associate \emph{keywords} compaiano nella tabella finale.

\section{Avvio procedura importazione}
L'intera procedura di importazione è stata automatizzata attraverso l'uso di Docker (e Docker Compose prevedendo l'integrazione con sistemi di generazione dashboard e altri software). Nel Capitolo \ref{auto_import} si trovano le informazioni per sfruttare questa tecnologia. Inoltre, nel caso si sia interessati a non utilizzare Docker e/o cambiare il modo in cui i dati originali sono stati trasformati, nei Capitoli \ref{manual_import} e \ref{scripts_update} sono disponibili un paio di paragrafi che coprono anche queste due necessità.

\section{Collaudo schema a stella}
Al fine di avere una qualche sicurezza che il lavoro svolto fino a questo punto fosse coerente con quello che era lo schema originale sono stati scritti $112$ test, non automatici, su 10 pubblicazioni che possono essere trovati nella cartella \emph{tests/} del progetto \cite{minhep_bitbucket}. Questi test consistono in alcune query che estraggono i dati dallo schema a stella. In fondo ad ogni test è presente una parte commentata che rappresenta l'output atteso da quella query. Questa parte commentata è ottenuta lanciando la query che si trova al di sopra e verificando che i risultati siano coerenti con le informazioni reperibili sul sito \url{http://hepdata.net}. Come al solito esaminiamo un esempio per chiarirci le idee. 

Supponiamo di essere interessati a estrarre tutti quei valori dell'asse $HERAPDF$ rispetto a $|\eta|$ compresi tra $[0.2,0.8]$ dalla prima tabella della pubblicazione con InspireID 1118047 e quindi eseguiamo:
\begin{lstlisting}[language=sql]
    select distinct Value
        from YAxisFacts yaf
        inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
        inner join XAxesDim xad on xad.ID = yaf.XAXIS_ID_1
    where InspireId = 1118047 and TableNumber = 1 
	    and xad.Header = '$|\\eta|$' 
        and yaf.Header = "HERAPDF" 
        and xad.LowValue >= 0.8 
        and xad.HighValue <= 2.0;
-- Value
-- 196
-- 181
-- 153
-- 140
-- 132
\end{lstlisting}
Se andiamo su \url{https://www.hepdata.net/record/ins1118047} e scorriamo la prima tabella ci troveremo di fronte ad una schermata del tipo mostrato in Figura \ref{extract_test_hepdata}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.6]{img/extract_test_hepdata.png}
	\caption{Estratto tabella hepdata.net}
	\label{extract_test_hepdata}
\end{figure}
dove sono evidenziati in blu i valori dell'asse X ($|\eta|$) e in rosso quelli dell'asse Y ($HERAPDF$). Come si può facilmente notare i valori riportati dalla query corrispondono perfettamente a quelli riportati da HEPData. Questo, insieme agli altri test, ci dà una certa fiducia sul lavoro fatto e sulla consistenza dei dati.